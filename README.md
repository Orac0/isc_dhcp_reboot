# isc_dhcp_reboot

Every time we replace a CPE, the DHCP has to reinitialize the leases (it remembers the MAC address of the device);
It reboots the service, rotates the dhcp.leases, and removes the logs older than 3 months according to the GDPR;