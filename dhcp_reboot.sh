#!/bin/bash

#
# 14/02/2018
#
# backs up the leases file and reboots the isc-dhcp service
# project tree:
#	|_ ./dhcp_reboot/
#		|_ dhcp_reboot.sh
#		|_ requirments.txt
#		 _ log/
#			|_ debug.log
#			|_ fail.log
#			|_ deleted_leases.log
#		|_ database/ (TODO)
#  
#
# must be ran as root!! CAUTION!! 
#

# TEST setup
#leases_path="/opt/parknet/dhcp_reboot/"
#active_lease="/opt/parknet/dhcp_reboot/dhcpd.leases"
#debug_log="./log/debug.log"
#delete_log="./log/deleted_leases.log"
#today_rotate_lease="/opt/parknet/dhcp_reboot/dhcpd.leases.$(date "+%d%m%Y")"



# PROD system path 
leases_path="/var/lib/dhcp/"
active_lease="/var/lib/dhcp/dhcpd.leases"
today_rotate_lease="/var/lib/dhcp/dhcpd.leases.$(date "+%d%m%Y")"
debug_log="/opt/parknet/dhcp_reboot/log/debug.log"
delete_log="/opt/parknet/dhcp_reboot/log/deleted_leases.log"

# DHCPD PID
isc="/var/run/dhcpd.pid"

function shutdown_isc 
{
	if [ -f "$isc" ]
	then
		/etc/init.d/isc-dhcp-server stop
		echo "$(date "+%d%m%Y %T") : Ive stopped the DHCP!"
	else
		echo "$(date "+%d%m%Y %T") : Your DHCP daemon is NOT running; Cant find the PID!" >> ${debug_log} 2>&1
		exit 1
	fi
}

function start_isc
{
	if [ $? -eq 0 ]
	then
		/etc/init.d/isc-dhcp-server start
		echo "$(date "+%d%m%Y %T") : Ive started the DHCP!"
	else
		echo "$(date "+%d%m%Y %T") : The previous function failed!; I could not rotate the lease!" >> ${debug_log} 2>&1
	fi
}

function overwrite_append_lease
{
	if [ -f "$today_rotate_lease" ]
	then
		
		# append data
		cat ${active_lease} >> ${today_rotate_lease}
		rm -vf ${active_lease} >> ${delete_log} 2>&1
	else
		# move data
		mv ${active_lease} ${today_rotate_lease}
	fi
}

function clear_lease_rotate
{
# find everything older than 3 months(131400minutes) 
# add everything we delete to the log
	files=$(find . -maxdepth 1 -type f -name "dhcpd.leases.*" -mmin +10)
	if [[ ${files[@]}  ]]
	then
		echo "$(date "+%d%m%Y %T") :" >> ${delete_log} 2>&1
		rm -vf ${files} >> ${delete_log} 2>&1
	else
		echo "$(date "+%d%m%Y %T") : Theres something wrong with the file array! I could not remove the old lease files !" >> ${debug_log} 2>&1
	fi
}

function new_leases
{
	if [ $? -eq 0 ] && [ ! -f ${active_lease} ]
	then
		touch ${active_lease}
	else
		echo "$(date "+%m%d%Y %T") : Your leases did not rotate today!" >> ${debug_log} 2>&1
	fi	
}

if [[ $EUID -ne 0 ]]
then
	echo "This script must be run as root!" 
else
	#echo "I will run the functions now!"
	shutdown_isc
	overwrite_append_lease
	new_leases
	start_isc
	clear_lease_rotate
	#echo "DONE!"
fi
